# DB-SERVICE-DEMO

## Overview

Simple java-based project to present:

* Sample project structure
* Spring boot 
* CRUD services
* Swagger
* Spring data
* Code First approach
* PostgreSQL integration
* JPQL 
* Unit testing
* Mockito
* Metrics enabling
* Static Web content serving
* Service Discovering enabling


## Usage

**Sample GET requests**
* <http://localhost:8889/ext-info>
* <http://localhost:8889/clients>
* <http://localhost:8889/clients/46>
* <http://localhost:8889/clients-ext?name=nextname15>
* <http://localhost:8889/products/1>
* <http://localhost:8889/clients/46/products>
* <http://localhost:8889/clients/34/products/1>

**swagger**
* http://localhost:8889/swagger-ui.html#/

**metrics**
* http://localhost:8889/actuator
* http://localhost:8889/actuator/metrics/process.cpu.usage
* http://localhost:8889/actuator/prometheus



## Tags

* 1.0.0           Simple RESTfull controler & project structure
* 1.1.0           Extension of SimpleControler
* 1.2.0           Swagger documentation
* 2.0.0           Full Rest API - CRUD 4 Clients & Products
* 2.1.0           Postman tests
* 2.2.0           Integration tests
* 2.3.0           Mockito-based tests
* 2.4.0           Actuator & Metrics
* 2.5.0           Serving Static Content


## Deployment 

**database component**

install [PostgreSQL DBMS](http://postgresql.org) to satisfy the following setting:
* spring.datasource.url=jdbc:postgresql://localhost:5433/demon
* spring.datasource.username=postgres
* spring.datasource.password=postgres


**spring-boot app**

```bash
git clone https://gitlab.com/draugustyn/db-service-demo.git
gradle build
gradle run
```
