package com.dbexample.demo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

import static springfox.documentation.builders.RequestHandlerSelectors.basePackage;
import static springfox.documentation.spi.DocumentationType.SWAGGER_2;

@EnableSwagger2
@Configuration
public class SwaggerConfiguration {
    private static final String BASE_PACKAGE = "com.dbexample.demo";

    @Bean
    public Docket productApi() {
        return new Docket(SWAGGER_2)
                .select()
                //.paths(PathSelectors.ant("/api/**")) // * , ?, **
                //.paths(PathSelectors.regex(("/^api/.*")))
                .apis(basePackage(BASE_PACKAGE))
                .build()
                .apiInfo(apiDetails())
                ;
    }


    private ApiInfo apiDetails() {
        return new ApiInfo("Microservice demo",
                "demo of a few features -  building microservice from scratch to ....",
                "1.1",
                "Free terms",
                new Contact("Dariusz Rafal Augustyn", "https://sites.google.com/site/draugustyn", "demo@demo.com"),
                "Free licence",
                "",
                Collections.emptyList());
    }
}