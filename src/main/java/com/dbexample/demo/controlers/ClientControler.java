package com.dbexample.demo.controlers;

import com.dbexample.demo.domain.Client;
import com.dbexample.demo.services.ClientService;
import com.dbexample.demo.services.ServiceErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.dbexample.demo.services.ServiceErrorCode.*;

/**
 *  Created by Dariusz Rafal Augustyn on 09.19.2019.
 */

@RestController
public class ClientControler {

    private static final Logger LOG = LoggerFactory.getLogger(ClientControler.class) ;

    @Autowired
    private ClientService clientService ;

    @GetMapping(value="/clients-echo")
    public String GetInfo() {
        String echo =  "echo: " + this.getClass().toString();
        LOG.info( echo);
        return (echo) ;
    }


    @GetMapping(value="/clients" )
    public List<Client> FindAll() {
        return clientService.FindAll();
    }

    @GetMapping(value="/clients-ext" )
    public List<Client> FindByName(@RequestParam(value = "name", defaultValue = "") String name) {
        if (name.isEmpty())
        {
            return clientService.FindAll();
        } else {
            return clientService.FindNamedClients(name);
        }
    }

    //@CrossOrigin()
    @GetMapping(value="/clients/{id:\\d*}")
    public ResponseEntity<Client> FindOne(@PathVariable("id") long id) {
        Client cli =  clientService.FindOneClient(id);
        final HttpStatus ret =  (cli == null)? HttpStatus.NOT_FOUND : HttpStatus.OK;
        return  ResponseEntity
                .status(ret)
                .body(cli);
    }

    @PostMapping(value="/clients")
    public ResponseEntity<Client> Create (@RequestBody Client cli_param) {
        Client cli =  clientService.CreateClient(cli_param);
        final HttpStatus ret =  (cli == null)? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.CREATED;
        return  ResponseEntity
                .status(ret)
                .body(cli);
    }

    @PutMapping(value="/clients/{id:\\d*}")
    public ResponseEntity<Client> Modify (@PathVariable("id") long id, @RequestBody Client cli_param) {
        cli_param.setId(id);
        ServiceErrorCode err = clientService.ModifyClient(cli_param);
        HttpStatus status ;
        switch (err) {
            case NOT_FOUND:
                status = HttpStatus.NOT_FOUND;
                break;
            case OK:
                status = HttpStatus.OK;
                break;
            default:
                status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return ResponseEntity.status(status).body(null);
    }

    // fake 2 inserts
    @PostMapping(value="/clients2")
    public ResponseEntity<Client> CreateTwoClients (@RequestBody Client cli_param) {
        Client cli =  clientService.CreateTwoClients(cli_param);
        final HttpStatus ret =  (cli == null)? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.CREATED;
        return  ResponseEntity
                .status(ret)
                .body(cli);
    }


}
