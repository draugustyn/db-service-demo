package com.dbexample.demo.controlers;

import com.dbexample.demo.domain.Element;
import com.dbexample.demo.exceptions.ElementNotFoundException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
public class ElementControler {

    @Value("${server.port}")
    private String port ;

    private Collection<Element> elCol;

    public ElementControler() {
        Element el1 = new Element(1, "one", 10);
        Element el2 = new Element(2, "two", 20);
        elCol = new ArrayList<Element>();
        elCol.add(el1);
        elCol.add(el2);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "Welcome to ElementController";
    }

    @RequestMapping(value ="/query", method = RequestMethod.GET)
    public String getQuery(@RequestParam(value = "name", defaultValue = "Port:") String name)
    {
        return ("response " + name  + port );
    }

    @RequestMapping(value = "/elements", method = RequestMethod.GET)
    public List<Element> GetElements()
    {
        return (List<Element>)elCol ;
    }

    @RequestMapping(value="/elements/{objId}", method = RequestMethod.GET)
    public Element GetOneElement (@PathVariable("objId") int objId)
    {
        Element _el = null;
        for (Element ee : elCol) {
            if (ee.getIdEl() == objId) {
                _el = ee;
                break;
            }
        }
        if (_el == null)
        {
            throw  new ElementNotFoundException((int)objId) ;
        }
        return (_el);
    }

    //headers = {"Content-type=application/json"}
    @RequestMapping(value="/elements/{elId}", method = RequestMethod.PUT )
    public Element ModifyElement (@RequestBody Element el,
                                  @PathVariable("elId") int objId)
    {
        Element _el = null;
        for (Element ee : elCol) {
            if (ee.getIdEl() == objId) {
                _el = ee;
                break;
            }
        }
        if (_el != null){
            _el.setIdEl(objId);
            _el.setNameEl(el.getNameEl());
            _el.setSizeEl(el.getSizeEl());
        }
        else
        {
            throw  new ElementNotFoundException((int)objId) ;
        }
        return (_el);
    }

    @RequestMapping(value="/elements", method = RequestMethod.POST )
    public Element CreateElement(@RequestBody Element el) {
        el.setIdEl(elCol.size());
        elCol.add(el) ;
        return (el);
    }
}