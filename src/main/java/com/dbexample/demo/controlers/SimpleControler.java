package com.dbexample.demo.controlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.UnknownHostException;
import static java.lang.Thread.currentThread;


/**
 *  Created by Dariusz Rafal Augustyn on 09.19.2019.
 */
@RestController
public class SimpleControler {

    private static final Logger LOG = LoggerFactory.getLogger(ClientControler.class) ;

    @RequestMapping(value="/info", method = RequestMethod.GET)
    public String GetInfo() {
        String  out = "Echo from GetInfo in object: "+  this.toString();
        LOG.info(out);
        return (out);
    }

    private static final String nullmsg = "none";

    @RequestMapping(value="/echo" , method = RequestMethod.GET)
    public String GetInfoWithParams(@RequestParam(value = "msg", defaultValue = nullmsg ) String message )
    {
        String out = "Echo from GetInfoWithParams in object " + this.toString();
        if (message.equals(nullmsg)) {
            LOG.info(out);
            return (out) ;
        }
         else
        {
            LOG.info( "Message "+ message + " -- "+ out);
            return (message);
        }
    }


    @Value("${server.port}")
    String port;

    private class ExtInfo
    {

        public String IPAddress;
        public String Port;
        public String ProcessID;
        public long ThreadID;

        public ExtInfo ()
        {
            IPAddress =  "0.0.0.0";
            Port = port;
            ProcessID = ManagementFactory.getRuntimeMXBean().getName().split("@")[0];
            ThreadID = currentThread().getId();

            try
            {
                IPAddress = InetAddress.getLocalHost().getHostAddress() ;
            }
            catch (UnknownHostException e)
            {
                e.printStackTrace();
            }
        }
    }

    @RequestMapping(value="/ext-info", method = RequestMethod.GET )
    public ExtInfo GetExtededInfo() {
        String methName = new Object(){}.getClass().getEnclosingMethod().getName();
        String out = "Echo from method " + methName + " in object " + this.toString();
        LOG.info(out);
        return (new ExtInfo());
    }


    @RequestMapping(value="/numbers/{nr:\\d+}", method = RequestMethod.GET)
    public String  GetNumber (@PathVariable("nr") int nr) {
        return ( Integer.toString(nr));
    }


    @RequestMapping (value = "/login", method = RequestMethod.GET)
    public String GetToken(@RequestHeader(value="Authorization", defaultValue = "none")  String auth ) {
        return ("Authorization value: "+ auth);
    }


    @RequestMapping (value="/inheaders", method = RequestMethod.GET)
    String ShowInHeader(@RequestHeader MultiValueMap<String, String> headers) {
        headers.forEach((key, value) -> {
            LOG.info(String.format("Header '%s' = %s", key, value));
        });
        return ("Number of header elements: " + headers.size());
    }


    @RequestMapping(value = "/my-resp-outheader", method = RequestMethod.GET)
    ResponseEntity<String> SetMyHeader() {
        return ResponseEntity.ok()
                .header("My-Header", "string")
                .body("My response header was set");
    }

    private class Result {
        private String  result_field;
        private String  result_mess;

        public Result(String result_field, String result_mess) {
            this.result_field = result_field;
            this.result_mess = result_mess;
        }

        public String getResult_field() {
            return result_field;
        }

        public void setResult_field(String result_field) {
            this.result_field = result_field;
        }

        public String getResult_mess() {
            return result_mess;
        }

        public void setResult_mess(String result_mess) {
            this.result_mess = result_mess;
        }

    }

    @RequestMapping(value = "/custom-result", method = RequestMethod.GET)
    ResponseEntity<Result> GetResult() {
        Result res = new Result("404", "errormessage123");
        return ResponseEntity.status(404)
                .header("My-Header", "object")
                .body(res);
    }

    @RequestMapping(value = "/manual", method = RequestMethod.GET)
    void SetManualRes(HttpServletResponse response) throws IOException {

        response.setHeader("My-Header", "foo");
        response.setStatus(200);
        response.getWriter().println("Hello World!");
    }


    @RequestMapping(value = "/manual-inc-header", method = RequestMethod.GET)
    void SetManualReqRes(HttpServletRequest request, HttpServletResponse response) throws IOException {

        Integer i = request.getIntHeader("cnt");
        response.setHeader("cnt",  (++i).toString());
        response.setStatus(200);
        response.getWriter().println("New cnt value:" + i);
    }
}
