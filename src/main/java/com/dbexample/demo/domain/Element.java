package com.dbexample.demo.domain;

/**
 *  Created by Dariusz Rafal Augustyn on 09.09.2019.
 */
public class Element {


    private long idEl ;
    private String nameEl ;
    private int sizeEl ;


    public Element()
    { }
    public Element (int _id, String _name, int _size)
    {
        idEl = _id ; nameEl = _name; sizeEl = _size ;
    }

    public long getIdEl() {
        return idEl;
    }
    public void setIdEl(long idEl) {
        this.idEl = idEl;
    }

    public String getNameEl() {
        return nameEl;
    }
    public void setNameEl(String nameEl) {
        this.nameEl = nameEl;
    }

    public int getSizeEl() {
        return sizeEl;
    }
    public void setSizeEl(int sizeEl) {
        this.sizeEl = sizeEl;
    }
}
