package com.dbexample.demo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Dariusz Rafal Augustyn on 09.09.2019.
 *
 * @note http://spring.io/guides/tutorials/bookmarks/
 */


@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Element not found")
public class ElementNotFoundException extends RuntimeException {

    public ElementNotFoundException(int objId) {
        super("Could not find  the object with id: '" + objId + "'.");
    }
}