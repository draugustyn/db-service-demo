package com.dbexample.demo.services;

import com.dbexample.demo.domain.Client;

import java.util.List;

/**
 *  Created by Dariusz Rafal Augustyn on 09.19.2019.
 */

public interface ClientService
{
    List<Client> FindAll() ;
    List <Client> FindNamedClients(String cliName);
    Client FindOneClient (Long id);
    Client CreateClient (Client cl);
    ServiceErrorCode ModifyClient (Client cl);
    Client CreateTwoClients (Client cl);
}
