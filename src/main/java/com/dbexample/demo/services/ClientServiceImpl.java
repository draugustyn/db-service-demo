package com.dbexample.demo.services;


import com.dbexample.demo.domain.Client;
import com.dbexample.demo.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;


/**
 *  Created by Dariusz Rafal Augustyn on 09.19.2019.
 */
@Service
//@Component
public class ClientServiceImpl implements ClientService {

/*
    List<Client> listClients  = Arrays.asList (
            new Client(1L, "name01", "sdname01" ),
            new Client(2L, "name02", "sdname02" ));
*/


    @Autowired
    private ClientRepository clientRepository;

    @Override
    public List<Client> FindAll() {
        //return  listClients ;

        return clientRepository.findAll() ;
    }

    @Override
    public List<Client> FindNamedClients(String cliName) {
        //return  listClients ;
        return clientRepository.findByName(cliName) ;

    }

    @Override
    public Client FindOneClient(Long id) {
        //return  (listClients.get(id.intValue()-1));
        Optional<Client> cli =  clientRepository.findById(id);
                if (cli.isPresent ())
                   return cli.get();
                else
                   return null;
    }

    @Override
    public Client CreateClient(Client cl) {
        return  clientRepository.save (cl);

    }

    public ServiceErrorCode ModifyClient (Client cli_param){
        Optional<Client> cli_res =  clientRepository.findById(cli_param.getId());
        if (!cli_res.isPresent()) {
            return ServiceErrorCode.NOT_FOUND;
        }
        Client cli = cli_res.get();

        cli.setName(cli_param.getName());
        cli.setSdname(cli_param.getSdname());

        cli = clientRepository.save(cli);
        if (cli == null) {
            return ServiceErrorCode.INTERNAL_SERVER_ERROR;
        }
        return ServiceErrorCode.OK;
    }


    // intentionally generates error (duplication of name value)
    @Override
    //@Transactional // on/off
    public Client CreateTwoClients(Client cl) {
        Client cl_ret ;
        cl_ret = clientRepository.save (cl);
        cl.setSdname(cl.getSdname()+"_2");
        cl_ret = clientRepository.save (cl);
        return  cl_ret ;
    }


}
