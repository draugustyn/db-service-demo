package com.dbexample.demo.services;

import com.dbexample.demo.domain.Product;
import java.util.List;


public interface ProductService {

    Product FindOneProduct (Long productId);
    Product FindOneProductOfClient (Long productId, Long clientId);
    List<Product> FindAllProducts();
    List<Product> FindProductsOfClient (Long clientId);
}
