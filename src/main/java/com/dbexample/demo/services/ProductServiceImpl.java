package com.dbexample.demo.services;

import com.dbexample.demo.domain.Product;
import com.dbexample.demo.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Optional;

@Component
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository ;

    @Override
    public Product FindOneProduct(Long productId) {
        Optional<Product> pro =  productRepository.findById(productId);
        if (pro.isPresent ())
            return pro.get();
        else
            return null;
    }

    @Override
    public Product FindOneProductOfClient(Long productId, Long clientId) {
        return null;
    }

    @Override
    public List<Product> FindAllProducts() {
        return (List<Product>) productRepository.findAll() ;
    }

    @Override
    public List<Product> FindProductsOfClient(Long clientId) {
        return productRepository.findProductByClientID(clientId) ;

    }
}
