package com.dbexample.demo.services;

public enum ServiceErrorCode {
    NOT_FOUND,
    OK,
    INTERNAL_SERVER_ERROR
}
