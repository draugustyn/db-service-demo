package com.dbexample.demo;


import com.dbexample.demo.domain.Product;
import com.dbexample.demo.repositories.ClientRepository;
import com.dbexample.demo.repositories.ProductRepository;
import com.dbexample.demo.services.ClientService;
import com.dbexample.demo.domain.Client;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import static junit.framework.Assert.*;


/**
 *  Created by Dariusz Rafal Augustyn on 09.19.2019.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DbDemoApplicationTests {

	@Autowired
	private ClientRepository clientRepository;


	@Autowired
	private ClientService clientService;


	@Test
	public void testContextLoads() {
		assertTrue("Never seen", true);
	}

	// service tests
	@Test
	public void  testClientServiceFindOne()
	{
		Client cli =  clientService.FindOneClient(46L);
		assertNotNull(cli);
	}

	@Test
	public void testClientServiceFindAll()
	{
	List<Client> li =  clientService.FindAll();
		Assert.assertFalse("Lista powinna być niepusta", li.isEmpty());
	}



	//repository tests
	@Test
	public void testPermanentCreationVeryfying(){
		int rnd =  new Random().nextInt(1000);
		String valueName =  "nextname" + rnd;
		String valueSdName =  "nextname" + rnd;
		Client cli =  new Client(valueName, valueSdName);
		//cli.setId(2L);

		cli = clientRepository.save(cli);

		List<Client> li =  clientRepository.findByName(valueName);
		Assert.assertEquals("Brak elementu o wskazanej nazwie",
				valueSdName,
				clientRepository.findByName(valueName).get(0).getSdname());
	}


	@Test
	public void testFindById(){
		// Given
		Long id = 46L;

		// When
		Optional<Client> optional_result = clientRepository.findById(id);

		// Then
		assertTrue("Brak elementu",optional_result.isPresent() );
	}

	@Test
	@Transactional // on/off
	public void testTemporaryCreationVeryfying() {

		//Given
		int rnd = new Random().nextInt(1000);
		String NameValue = "nextnametemp" + rnd;
		String sdNameValue = "nextsdname" + rnd;
		Client cli = new Client(NameValue, sdNameValue );

		//When
		cli = clientRepository.save(cli);

		//Then
		List<Client> li = clientRepository.findByName(NameValue);
		Assert.assertEquals("Brak elementu o wskazanej nazwie",
				             sdNameValue,
				             li.get(0).getSdname());
	}

	@Test
	public void testFindVerifying() {

		Client cl;
		List <Client> clist ;
		clist = clientRepository.findAll();

		final String pattern = "%11%";
		clist = clientRepository.findBySdnameLike(pattern) ;
		// Assert.assertEquals("Zła liczba elementów", 1, cls.size());

        clist = clientRepository.findByNameAndSdname("nextname15", "nextname15");
		Assert.assertEquals("Zła liczba elementów", 1, clist.size());

		clist = clientRepository.findByNameOrSdname ("name12", "sdname12XX") ;
		Assert.assertEquals("Zła liczba elementów", 0, clist.size());


		clist =  clientRepository.findByNameIgnoreCase("NEXTNAME15");

		clist =  clientRepository.findByIdBetween(100, 350);

		cl =  clientRepository.findFirstByName("nextname105");

		clist = clientRepository.findFirst3ByNameContaining("1");

		final String pattern2 = "next%15";
		clist = clientRepository.findBySdnameLikeSQLbased(pattern2) ;
		Assert.assertNotEquals("Zła liczba elementów", 0, clist.size());

		clist = null ;

		clist = clientRepository.findByLikesSQLbased ( "n%", "%s%") ;
		Assert.assertNotEquals("Zła liczba elementów", 0, clist.size());
	}

	@Test
	public void testJpaRepositoryFindVerify(){
		List<Client>  clist;
		clist = clientRepository.findAll(new Sort(Sort.Direction.ASC, "name"));

		Page<Client> clip = clientRepository.findAll (PageRequest.of(2,4, new Sort (Sort.Direction.ASC, "sdname")));
		long i;
		i = clip.getTotalElements();
		i =  clip.getTotalPages();
		i = clip.getNumber();
		i =  clip.getNumberOfElements();
		clist = clip.getContent();
	}



	@Autowired
	private ProductRepository productRepository;

	@Test
	//LAZY @Transactional EAGER
	public void testMany2OneRelation (){
		Optional<Client> opt_cli_no_46 = clientRepository.findById(46L) ;
		Client cli_no_46 = opt_cli_no_46.get();


		//int nsize =  cli_no_46.getProducts().size();
		List<Product> prolist = cli_no_46.getProducts();

		List<Product> prolist2 ;
		prolist2  =  productRepository.findByClient(cli_no_46);
		cli_no_46.setProducts(prolist2);
		int nsize =  cli_no_46.getProducts().size();

		Client cl_and_pro_no_46 =  clientRepository.findFullById(46L).get(0);
	}
}
