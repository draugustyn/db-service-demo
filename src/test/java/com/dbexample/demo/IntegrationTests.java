package com.dbexample.demo;

import com.dbexample.demo.domain.Client;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class IntegrationTests {

    @Autowired
    private TestRestTemplate restTemplate ;

    @Test
    public void testClientFindAll()
    {
        // arrange
        String url = "/clients";

        // act
        ResponseEntity<Client[]> res = restTemplate.getForEntity(url, Client[].class);
        List<Client> cli = Arrays.asList(res.getBody());

        // assert
        Assert.assertEquals(res.getStatusCode(), HttpStatus.OK);
        Assert.assertTrue(cli.size()>0);
        Assertions.assertThat(res.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

}
