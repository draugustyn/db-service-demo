package com.dbexample.demo;

import com.dbexample.demo.domain.Client;
import com.dbexample.demo.repositories.ClientRepository;
import com.dbexample.demo.services.ClientService;
import com.dbexample.demo.services.ClientServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class MockedServiceTests {

    @Mock
    private ClientRepository clientRepository;

    @InjectMocks
    private ClientServiceImpl clientService ;

    @Test
    public void testMockedService()
    {
        /// Arrange
        long id = 46;
        String cliname =  "SomeMockName";

        Client cli =  mock(Client.class);
        when(cli.getId()).thenReturn(id);
        when(cli.getName()).thenReturn(cliname);
        when(cli.getProducts()).thenReturn(null);
        // cli =  new Client(id, cliname,cliname);

        when(clientRepository.findById(id)).thenReturn(Optional.of(cli));
        when(clientRepository.findFirstByName(cliname)).thenReturn(cli);

        // Action
        Client cli_ret =  clientService.FindOneClient(id);

        // Assert
        Assert.assertNotNull(cli_ret);
//      Assert.assertEquals(cliname, cli_ret.getName());
        verify(clientRepository).findById(id);
//      verify(clientRepository,times(2)).findFirstByName(cliname);
        verify(clientRepository, atMost(2)).findAll();
    }
}
